package com.example.herlin.projectplbtw;

public interface OnLoadMoreListener {
    void onLoadMore();
}
