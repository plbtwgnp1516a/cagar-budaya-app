package com.example.herlin.projectplbtw.detailreward.api;

import com.example.herlin.projectplbtw.detailreward.model.DetailRewardResponse;

import retrofit.Call;
import retrofit.http.DELETE;
import retrofit.http.Path;

/**
 * Created by VAIO on 5/31/2016.
 */
public interface ApiDetailReward {
    @DELETE("rewards/{id}")
    Call<DetailRewardResponse> deleteReward(@Path("id") String id);
}
