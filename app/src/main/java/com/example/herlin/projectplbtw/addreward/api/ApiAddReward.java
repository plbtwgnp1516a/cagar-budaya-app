package com.example.herlin.projectplbtw.addreward.api;

import com.example.herlin.projectplbtw.addreward.model.AddRewardModel;
import com.example.herlin.projectplbtw.addreward.model.AddRewardResponse;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by VAIO on 5/21/2016.
 */
public interface ApiAddReward {
    @POST("rewards")
    Call<AddRewardResponse> postReward(@Body AddRewardModel addChallengeModel);
}
