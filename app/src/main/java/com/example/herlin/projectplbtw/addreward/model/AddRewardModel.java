package com.example.herlin.projectplbtw.addreward.model;

/**
 * Created by VAIO on 5/21/2016.
 */
public class AddRewardModel {
    private String nama_reward;
    private String deskripsi;
    private String minimal_poin;

    public String getNama_reward() {
        return nama_reward;
    }

    public void setNama_reward(String nama_reward) {
        this.nama_reward = nama_reward;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getMinimal_poin() {
        return minimal_poin;
    }

    public void setMinimal_poin(String minimal_poin) {
        this.minimal_poin = minimal_poin;
    }
}
