package com.example.herlin.projectplbtw.login.model;

/**
 * Created by VAIO on 5/22/2016.
 */
public class LoginModel {
    private String api_key;
    private String email;
    private String password;

    public String getApi_key() {
        return api_key;
    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
