package com.example.herlin.projectplbtw.editreward.model;

/**
 * Created by VAIO on 6/1/2016.
 */
public class EditRewardResponse {
    private Boolean Success;
    private String Info;

    public String getInfo() {
        return Info;
    }

    public void setInfo(String info) {
        Info = info;
    }

    public Boolean getSuccess() {
        return Success;
    }

    public void setSuccess(Boolean success) {
        Success = success;
    }

}
