package com.example.herlin.projectplbtw.addherritage.userinterface;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.herlin.projectplbtw.R;
import com.example.herlin.projectplbtw.ServiceGenerator;
import com.example.herlin.projectplbtw.addherritage.api.ApiAddHerritage;
import com.example.herlin.projectplbtw.addherritage.model.AddHerritageModel;
import com.example.herlin.projectplbtw.addherritage.model.AddHerritageResponse;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class AddHerritageActivity extends AppCompatActivity {
    private ApiAddHerritage apiAddHerritage = null;
    public static final String API_KEY="asdfghjkl";
    @Bind(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    @Bind(R.id.textNamaCagar)
    EditText textNamaCagar;
    @Bind(R.id.textDeskripsi)
    EditText textDeskripsi;
    @Bind(R.id.textTahun)
    EditText textTahun;
    @Bind(R.id.spinJenis)
    Spinner spinJenis;
    @Bind(R.id.textAlamat)
    EditText textAlamat;
    @Bind(R.id.imageThumbnail)
    ImageView imageThumbnail;
    @Bind(R.id.btnCancel)
    Button btnCancel;
    @Bind(R.id.btnAddCagar)
    Button btnAddCagar;
    @Bind(R.id.btnSelectImage)
    Button btnSelectImage;

    @OnClick(R.id.btnCancel)
    void backToMenu(View view) {
        this.finish();
    }

    @OnClick(R.id.btnAddCagar)
    void setOnClickBtnAddCagar(View view) {
        if (textAlamat.getText().toString().isEmpty() || textDeskripsi.getText().toString().isEmpty() || textNamaCagar.getText().toString().isEmpty() || textTahun.getText().toString().isEmpty()) {
            Snackbar.make(view, "Please fill the blank field!", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        }
        else
        {
            final ProgressDialog dialog = new ProgressDialog(AddHerritageActivity.this);
            dialog.setMessage("Please wait...");
            dialog.setCancelable(false);
            dialog.show();
            Call<AddHerritageResponse> call = null;

            AddHerritageModel addHerritageModel = new AddHerritageModel();
            addHerritageModel.setApi_key(API_KEY);
            addHerritageModel.setAlamat(textAlamat.getText().toString());
            addHerritageModel.setDeskripsi(textDeskripsi.getText().toString());
            addHerritageModel.setId_user("1"); //harus dr shared preference
            addHerritageModel.setLatitude("123"); //harus dr maps
            addHerritageModel.setLongitude("456");  //harus dr maps
            addHerritageModel.setNama_cagar(textNamaCagar.getText().toString());
            addHerritageModel.setStatus("waiting for confirmation");
            addHerritageModel.setTahun_peninggalan(textTahun.getText().toString());
            addHerritageModel.setNama_jenis(spinJenis.getSelectedItem().toString());

            call = apiAddHerritage.postHerritage(addHerritageModel);

            call.enqueue(new Callback<AddHerritageResponse>() {
                @Override
                public void onResponse(Response<AddHerritageResponse> response, Retrofit retrofit) {
                    dialog.dismiss();
                    Toast.makeText(AddHerritageActivity.this, response.code() + "", Toast.LENGTH_SHORT).show();
                    if (2 == response.code() / 100) {
                       // Toast.makeText(LoginActivity.this, response.code() + " ", Toast.LENGTH_SHORT).show();
                        if (response.body().getSuccess() == true)
                            moveToMenu();

                        else
                            Snackbar.make(coordinatorLayout, "Add Herritage Error", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                    } else {
                        Snackbar.make(coordinatorLayout, "Add Herritage Error", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    dialog.dismiss();
                    Snackbar.make(coordinatorLayout, t.getMessage()+"Add Herritage Failed", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
               //     textNamaCagar.setText(t.getMessage()+"");

                }
            });
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_herritage);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Add Herritage");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        apiAddHerritage = ServiceGenerator.createService(ApiAddHerritage.class);
    }

    private void moveToMenu()
    {
        Toast.makeText(AddHerritageActivity.this, "Successfully add herritage!", Toast.LENGTH_SHORT).show();
        finish();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
