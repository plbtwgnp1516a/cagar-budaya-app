package com.example.herlin.projectplbtw.register.userinterface;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.herlin.projectplbtw.R;
import com.example.herlin.projectplbtw.ServiceGenerator;
import com.example.herlin.projectplbtw.login.userinterface.LoginActivity;
import com.example.herlin.projectplbtw.register.api.ApiRegister;
import com.example.herlin.projectplbtw.register.model.RegisterModel;
import com.example.herlin.projectplbtw.register.model.RegisterResponse;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Herlin on 5/9/2016.
 */
public class RegisterActivity extends Activity {

    public static final String API_KEY="asdfghjkl";
    private ApiRegister apiRegister = null;

    private static final String TAG = RegisterActivity.class.getSimpleName();
    @Bind(R.id.btnRegister)
    Button btnRegister;
    @Bind(R.id.btnLinkToLoginScreen)
    Button btnLinkToLogin;
    @Bind(R.id.textName)
    EditText textName;
    @Bind(R.id.textEmail)
    EditText textEmail;
    @Bind(R.id.textPassword)
    EditText textPassword;
    @Bind(R.id.textTelepon)
    EditText textTelepon;

    private ProgressDialog pDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        apiRegister = ServiceGenerator.createService(ApiRegister.class);

        // Register Button Click event
        btnRegister.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String name = textName.getText().toString();
                String email = textEmail.getText().toString();
                String password = textPassword.getText().toString();
                String telepon = textTelepon.getText().toString();

                if (!name.isEmpty() && !email.isEmpty() && !password.isEmpty()) {
                    registerUser(name, email, password,telepon);
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please enter your details!", Toast.LENGTH_LONG)
                            .show();
                }
            }
        });

        // Link to Login Screen
        btnLinkToLogin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
              moveToLogin();
            }
        });
    }
    private void registerUser(String name, String email, String password,String telepon)
    {
        final ProgressDialog dialog = new ProgressDialog(RegisterActivity.this);
        dialog.setMessage("Please wait...");
        dialog.setCancelable(false);
        dialog.show();
        Call<RegisterResponse> call = null;

        RegisterModel registerModel = new RegisterModel();
        registerModel.setEmail(email);
        registerModel.setTelepon(telepon);
        registerModel.setPassword(password);
        registerModel.setNama(name);
        registerModel.setNama_role("user");
        registerModel.setPoin("0");
        call = apiRegister.postRegister(registerModel);
        call.enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Response<RegisterResponse> response, Retrofit retrofit) {
                dialog.dismiss();
                Toast.makeText(RegisterActivity.this, response.code()+" "+response.body(), Toast.LENGTH_SHORT).show();
                if (2 == response.code() / 100) {

                                Toast.makeText(RegisterActivity.this, "response code 200", Toast.LENGTH_SHORT).show();
                                if(response.body().getSuccess()==true) {
                                    Toast.makeText(RegisterActivity.this, "New User Successfully Added", Toast.LENGTH_SHORT).show();
                                    moveToLogin();
                                }
                                else
                                    Toast.makeText(RegisterActivity.this, "Register Failed200", Toast.LENGTH_SHORT).show();

                            } else {
                                Toast.makeText(RegisterActivity.this, "Register Failed", Toast.LENGTH_SHORT).show();

                            }
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(RegisterActivity.this, "Failure Happnened", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
    }
    private void moveToLogin()
    {

        Intent i = new Intent(getApplicationContext(),
                LoginActivity.class);
        startActivity(i);
        finish();
    }

}
