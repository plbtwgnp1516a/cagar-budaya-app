package com.example.herlin.projectplbtw.challenge.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.herlin.projectplbtw.R;
import com.example.herlin.projectplbtw.ServiceGenerator;
import com.example.herlin.projectplbtw.challenge.api.ApiChallenge;
import com.example.herlin.projectplbtw.challenge.model.ChallengeModel;
import com.example.herlin.projectplbtw.challenge.userinterface.ChallengeActivity;
import com.example.herlin.projectplbtw.detailchallenge.userinterface.DetailChallengeActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by VAIO on 5/17/2016.
 */
public class RecyclerChallengeAdapter extends RecyclerView.Adapter {
    private static final String idUserMarker = "id_user_marker";
    private static final String loginMarker = "login_marker";
    private static final String namaMarker = "nama_marker";
    private static final String emailMarker = "email_marker";
    private static final String poinMarker = "poin_marker";
    private static final String teleponMarker = "telepon_marker";
    private static final String roleMarker = "role_marker";
    private static SharedPreferences sharedPreferences;
    private static final String sharedPreferenceName = "shared_preference";


    private ChallengeActivity challengeActivity;
    public ApiChallenge apiChallenge = null;
    private final int TYPE_CHALLENGE = 1;
    private Context context;
    private LayoutInflater inflater;
    private List<ChallengeModel> challengeModelList;
    private CoordinatorLayout coordinatorLayout;
    private ChallengeViewHolder challengeViewHolder;
    public void setChallengeActivity(ChallengeActivity challengeActivity) {
        this.challengeActivity = challengeActivity;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setInflater(LayoutInflater inflater) {
        this.inflater = inflater;
    }

    public RecyclerChallengeAdapter
            (List<ChallengeModel> challengeModelList, RecyclerView recyclerView) {
        apiChallenge = ServiceGenerator.createService(ApiChallenge.class);
        this.challengeModelList = challengeModelList;

    }

    @Override
    public int getItemViewType(int position) {
        return TYPE_CHALLENGE;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layoutId;
        View view;
        layoutId = R.layout.item_challenge_list;
        view = LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);
        return new ChallengeViewHolder(view);

    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        setChallengeViewHolder(holder, position);



    }

    public void setCoordinatorLayout(CoordinatorLayout coordinatorLayout) {
        this.coordinatorLayout = coordinatorLayout;
    }


    public void setChallengeViewHolder(RecyclerView.ViewHolder holder,final int position) {
        challengeViewHolder = (ChallengeViewHolder) holder;


        challengeViewHolder.challengeModel = challengeModelList.get(position);
        sharedPreferences = holder.itemView.getContext().getSharedPreferences(sharedPreferenceName, 0);

        challengeViewHolder.textNamaChallenge.setText(challengeViewHolder
                .challengeModel.getNama_challenge()+"");

        Picasso.with(holder.itemView.getContext()).load(challengeViewHolder.challengeModel.getNama_challenge())
                .placeholder(R.drawable.cabud4).into(challengeViewHolder.imageChallenge);


            challengeViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle b = new Bundle();
                    b.putString("nama_challenge",challengeModelList.get(position).getNama_challenge());
                    b.putString("deskripsi",challengeModelList.get(position).getDeskripsi());
                    b.putString("id_challenge",challengeModelList.get(position).getId_challenge());

                    Intent i = new Intent(v.getContext(), DetailChallengeActivity.class);
                    i.putExtras(b);
                    challengeActivity.startActivity(i);

                }
            });
    }

    @Override
    public int getItemCount() {
        return (challengeModelList.size());
    }


    public static class ChallengeViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.textNamaChallenge)
        TextView textNamaChallenge;

        @Bind(R.id.imageChallenge)
        ImageView imageChallenge;

        ChallengeModel challengeModel;

        public ChallengeViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }
}
