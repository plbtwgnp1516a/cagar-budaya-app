package com.example.herlin.projectplbtw.editchallenge.userinterface;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.herlin.projectplbtw.R;
import com.example.herlin.projectplbtw.ServiceGenerator;
import com.example.herlin.projectplbtw.editchallenge.api.ApiEditChallenge;
import com.example.herlin.projectplbtw.editchallenge.model.EditChallengeModel;
import com.example.herlin.projectplbtw.editchallenge.model.EditChallengeResponse;
import com.example.herlin.projectplbtw.editreward.userinterface.EditRewardActivity;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class EditChallengeActivity extends AppCompatActivity {
    private String nama_challenge;
    private String deskripsi;
    private String id_challenge;

    private ApiEditChallenge apiEditChallenge = null;
    @Bind(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    @Bind(R.id.textNamaChallenge)
    EditText textNamaChallenge;
    @Bind(R.id.textDeskripsi)
    EditText textDeskripsi;
    @Bind(R.id.imageThumbnail)
    ImageView imageThumbnail;
    @Bind(R.id.btnCancel)
    Button btnCancel;
    @Bind(R.id.btnEditChallenge)
    Button btnEditChallenge;
    @OnClick(R.id.btnCancel)
    void backToMenu(View view) {
        this.finish();
    }
    @OnClick(R.id.btnEditChallenge)
    void setOnClickBtnEditChallenge(View view)
    {
        if(textNamaChallenge.getText().toString().isEmpty()||textDeskripsi.getText().toString().isEmpty())
        {
            Snackbar.make(view, "Please fill the blank field!", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        }
        else
        {
            final ProgressDialog dialog = new ProgressDialog(EditChallengeActivity.this);
            dialog.setMessage("Please wait...");
            dialog.setCancelable(false);
            dialog.show();
            Call<EditChallengeResponse> call = null;

            EditChallengeModel editChallengeModel = new EditChallengeModel();
            editChallengeModel.setNama_challenge(textNamaChallenge.getText().toString());
            editChallengeModel.setDeskripsi(textDeskripsi.getText().toString());

            call = apiEditChallenge.editChallenge(id_challenge,editChallengeModel);

            call.enqueue(new Callback<EditChallengeResponse>() {
                @Override
                public void onResponse(Response<EditChallengeResponse> response, Retrofit retrofit) {
                    dialog.dismiss();
                    Toast.makeText(EditChallengeActivity.this, response.code() + "", Toast.LENGTH_SHORT).show();
                    if (2 == response.code() / 100) {
                        // Toast.makeText(LoginActivity.this, response.code() + " ", Toast.LENGTH_SHORT).show();
                        if (response.body().getSuccess() == true)
                            moveToMenu();

                        else
                            Snackbar.make(coordinatorLayout, "Edit Challenge Error", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                    } else {
                        Snackbar.make(coordinatorLayout, "Edit Challenge Error", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    dialog.dismiss();
                    Snackbar.make(coordinatorLayout, t.getMessage()+"Edit Challenge Failed", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    // textNamaCagar.setText(t.getMessage()+"");

                }
            });
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_challenge);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Edit Challenge");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        apiEditChallenge = ServiceGenerator.createService(ApiEditChallenge.class);
        Bundle b = getIntent().getExtras();
        nama_challenge=b.getString("nama_challenge");
        id_challenge=b.getString("id_challenge");
        deskripsi=b.getString("deskripsi_challenge");
        textDeskripsi.setText(deskripsi);
        textNamaChallenge.setText(nama_challenge);
    }

    private void moveToMenu()
    {
        Toast.makeText(EditChallengeActivity.this, "Successfully add challenge!", Toast.LENGTH_SHORT).show();
        finish();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
