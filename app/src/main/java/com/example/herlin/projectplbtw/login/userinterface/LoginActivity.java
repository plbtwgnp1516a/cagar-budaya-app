package com.example.herlin.projectplbtw.login.userinterface;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.herlin.projectplbtw.AdminActivity;
import com.example.herlin.projectplbtw.MainActivity;
import com.example.herlin.projectplbtw.R;
import com.example.herlin.projectplbtw.ServiceGenerator;
import com.example.herlin.projectplbtw.login.api.ApiLogin;
import com.example.herlin.projectplbtw.login.model.LoginModel;
import com.example.herlin.projectplbtw.login.model.LoginResponse;
import com.example.herlin.projectplbtw.register.userinterface.RegisterActivity;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Herlin on 5/9/2016.
 */
public class LoginActivity extends Activity {
    private static final String idUserMarker = "id_user_marker";
    private static final String loginMarker = "login_marker";
    private static final String namaMarker = "nama_marker";
    private static final String emailMarker = "email_marker";
    private static final String poinMarker = "poin_marker";
    private static final String teleponMarker = "telepon_marker";
    private static final String roleMarker = "role_marker";
    private static SharedPreferences sharedPreferences;
    private static final String sharedPreferenceName = "shared_preference";
    public static final String API_KEY = "asdfghjkl";
    private ApiLogin apiLogin = null;
    @Bind(R.id.btnLinkToRegisterScreen)
    Button btnLinkToRegister;
    @Bind(R.id.btnLogin)
    Button btnLogin;
    @Bind(R.id.textEmail)
    TextView textEmail;
    @Bind(R.id.textPassword)
    TextView textPassword;
    @Bind(R.id.textWarning)
    TextView textWarning;
    @Bind(R.id.imageWarning)
    ImageView imageWarning;

    @OnClick(R.id.btnLinkToRegisterScreen)
    void setOnClickLinkToRegister(View view) {
        Intent i = new Intent(getApplicationContext(),
                RegisterActivity.class);
        startActivity(i);
        finish();
    }

    @OnClick(R.id.btnLogin)
    void setOnClickLoginButton(View view) {
        if (textEmail.getText().toString().isEmpty() || textPassword.getText().toString().isEmpty()) {
            Toast.makeText(LoginActivity.this, "Email and Password must be filled", Toast.LENGTH_SHORT).show();
        } else {
            final ProgressDialog dialog = new ProgressDialog(LoginActivity.this);
            dialog.setMessage("Please wait...");
            dialog.setCancelable(false);
            dialog.show();
            Call<LoginResponse> call = null;

            LoginModel loginModel = new LoginModel();
            loginModel.setApi_key(API_KEY);
            loginModel.setEmail(textEmail.getText().toString());
            loginModel.setPassword(textPassword.getText().toString());
            call = apiLogin.postLogin(loginModel);

            call.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Response<LoginResponse> response, Retrofit retrofit) {
                    dialog.dismiss();
                    Toast.makeText(LoginActivity.this, response.code() + "", Toast.LENGTH_SHORT).show();
                    if (2 == response.code() / 100) {
                        Toast.makeText(LoginActivity.this, response.code() + " ", Toast.LENGTH_SHORT).show();
                        if (response.body().getInfo() == null) {
                            if (response.body().getContent().get(0).getId_role().equalsIgnoreCase("1"))
                                moveToMenuUser(response);
                            else if (response.body().getContent().get(0).getId_role().equalsIgnoreCase("2"))
                                moveToMenuAdmin(response);
                            else
                                showErrorMessage();
                        }
                        else
                        {
                            showErrorMessage();
                        }
                    } else {
                        showErrorMessage();
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    dialog.dismiss();
                    textWarning.setText("Login Failed");
                    textWarning.setVisibility(View.VISIBLE);
                    imageWarning.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        ButterKnife.bind(this);

        textWarning.setVisibility(View.INVISIBLE);
        imageWarning.setVisibility(View.INVISIBLE);
        apiLogin = ServiceGenerator.createService(ApiLogin.class);

        sharedPreferences = getSharedPreferences(sharedPreferenceName, 0);
        boolean isLoggedIn = sharedPreferences.getBoolean(loginMarker, false);
        String role = sharedPreferences.getString(roleMarker, "");
        if (isLoggedIn) {
            if (role.equalsIgnoreCase("1")) {
                Intent i = new Intent(getApplicationContext(),
                        MainActivity.class);
                startActivity(i);
                finish();
            } else if (role.equalsIgnoreCase("2")) {
                Intent i = new Intent(getApplicationContext(),
                        AdminActivity.class);
                startActivity(i);
                finish();
            } else {

            }
        }
    }

    private void showErrorMessage() {
        textWarning.setText("Username or Password is incorrect");
        textWarning.setVisibility(View.VISIBLE);
        imageWarning.setVisibility(View.VISIBLE);
    }

    private void moveToMenuUser(Response<LoginResponse> response) {

        LoginResponse loginResponse = response.body();
        String nama = loginResponse.getContent().get(0).getNama();
        String email = loginResponse.getContent().get(0).getEmail();
        String poin = loginResponse.getContent().get(0).getPoin();
        String telepon = loginResponse.getContent().get(0).getTelepon();
        String role = loginResponse.getContent().get(0).getId_role();
        String idUser = loginResponse.getContent().get(0).getId_user();
        saveSharedPreference(nama, email, poin, telepon, role, idUser);
        Intent i = new Intent(getApplicationContext(),
                MainActivity.class);
        startActivity(i);
        finish();
    }

    private void moveToMenuAdmin(Response<LoginResponse> response) {

        LoginResponse loginResponse = response.body();
        String nama = loginResponse.getContent().get(0).getNama();
        String email = loginResponse.getContent().get(0).getEmail();
        String poin = loginResponse.getContent().get(0).getPoin();
        String telepon = loginResponse.getContent().get(0).getTelepon();
        String role = loginResponse.getContent().get(0).getId_role();
        String idUser = loginResponse.getContent().get(0).getId_user();
        saveSharedPreference(nama, email, poin, telepon, role, idUser);
        Intent i = new Intent(getApplicationContext(),
                AdminActivity.class);
        startActivity(i);
        finish();
    }


    private void saveSharedPreference(String nama, String email, String poin, String telepon, String role, String idUser) {
        SharedPreferences.Editor e = sharedPreferences.edit();
        e.putBoolean(this.loginMarker, true);
        e.putString(namaMarker, nama);
        e.putString(emailMarker, email);
        e.putString(poinMarker, poin);
        e.putString(teleponMarker, telepon);
        e.putString(roleMarker, role);
        e.putString(idUserMarker, idUser);
        e.commit();
    }
}
