package com.example.herlin.projectplbtw.challenge.userinterface;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.example.herlin.projectplbtw.R;
import com.example.herlin.projectplbtw.ServiceGenerator;
import com.example.herlin.projectplbtw.addchallenge.userinterface.AddChallengeActivity;
import com.example.herlin.projectplbtw.addherritage.userinterface.AddHerritageActivity;
import com.example.herlin.projectplbtw.challenge.adapter.RecyclerChallengeAdapter;
import com.example.herlin.projectplbtw.challenge.api.ApiChallenge;
import com.example.herlin.projectplbtw.challenge.model.ChallengeModel;
import com.example.herlin.projectplbtw.challenge.model.ChallengeResponse;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class ChallengeActivity extends Fragment {
    private static final String idUserMarker = "id_user_marker";
    private static final String loginMarker = "login_marker";
    private static final String namaMarker = "nama_marker";
    private static final String emailMarker = "email_marker";
    private static final String poinMarker = "poin_marker";
    private static final String teleponMarker = "telepon_marker";

    private static final String roleMarker = "role_marker";
    private static SharedPreferences sharedPreferences;
    private static final String sharedPreferenceName = "shared_preference";



    View rootview;

    public static final String API_KEY="asdfghjkl";
    @Bind(R.id.progressBar)
    ProgressBar progressBar;
    @Bind(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
 //   @Bind(R.id.contentChallengeList)
 //   LinearLayout contentChallengeList;

    @Bind(R.id.listChallenge)
    RecyclerView recyclerView;
    @Bind(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    private List<ChallengeModel> challengeModelList;
    private ApiChallenge apiChallenge = null;
    private RecyclerChallengeAdapter adapter;

    @Bind(R.id.fab)
    FloatingActionButton fab;

    @OnClick(R.id.fab)
    void addChallenge(View view) {
        Intent i=new Intent(getContext().getApplicationContext(), AddChallengeActivity.class);
        startActivity(i);
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreate(savedInstanceState);
        rootview= inflater.inflate(R.layout.activity_challenge, container, false);
        getActivity().setTitle("Challenge");
        ButterKnife.bind(this, rootview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(
                new LinearLayoutManager(getActivity().getApplicationContext()));



        progressBar.setVisibility(View.VISIBLE);

        sharedPreferences = getActivity().getSharedPreferences(sharedPreferenceName, 0);
        String role = sharedPreferences.getString(roleMarker,"0");
        if(role.equalsIgnoreCase("2"))
        {
            fab.setVisibility(View.VISIBLE);
        }
        else
        {
            fab.setVisibility(View.INVISIBLE);
        }

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (adapter == null) {
                    progressBar.setVisibility(View.VISIBLE);
                }
                getChallengeList();
            }
        });
        apiChallenge = ServiceGenerator
                .createService(ApiChallenge.class);
        getChallengeList();

        return rootview;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void refreshPushNotification() {
        progressBar.setVisibility(View.VISIBLE);
  //      contentChallengeList.setVisibility(View.INVISIBLE);
        getChallengeList();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (9 == resultCode && 9 == requestCode) {
            refreshPushNotification();
            Snackbar.make(coordinatorLayout, "Push Notification Edited", Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        getChallengeList();
        Snackbar.make(coordinatorLayout, "Challenge Berhasil direfresh", Snackbar.LENGTH_LONG).show();
    }

    private void getChallengeList() {
        progressBar.setVisibility(View.VISIBLE);
        Call<ChallengeResponse> call = null;
        call = apiChallenge.getChallengeList("1","1000",API_KEY);

//        call = apiReward.getHerritageList();
        call.enqueue(new Callback<ChallengeResponse>() {
            @Override
            public void onResponse(Response<ChallengeResponse> response,
                                   Retrofit retrofit) {

                if (2 == response.code() / 100 && response.isSuccess()) {
                    showChallengeList(response);
                } else {
                    showErrorMessage();
                }
                progressBar.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
        //        contentChallengeList.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Throwable t) {
                Snackbar.make(coordinatorLayout, "Failed to Load Content",
                        Snackbar.LENGTH_LONG).show();
                progressBar.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
       //         contentChallengeList.setVisibility(View.VISIBLE);
            }
        });
    }

    private void showErrorMessage() {
        Snackbar.make(coordinatorLayout, "Error to Load Content", Snackbar.LENGTH_LONG).show();
    }

    private void showChallengeList
            (Response<ChallengeResponse> response) {

        final ChallengeResponse challengeResponse = response.body();
        challengeModelList = challengeResponse.getListChallenge();
        if(challengeModelList!=null) {
            adapter = new RecyclerChallengeAdapter(challengeModelList, recyclerView);
            adapter.setCoordinatorLayout(coordinatorLayout);
            adapter.setContext(getContext().getApplicationContext());
            adapter.setInflater(getActivity().getLayoutInflater());
            adapter.setChallengeActivity(this);
            recyclerView.setAdapter(adapter);
        }

    }
}
