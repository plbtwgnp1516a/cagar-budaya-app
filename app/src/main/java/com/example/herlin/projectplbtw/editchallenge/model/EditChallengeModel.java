package com.example.herlin.projectplbtw.editchallenge.model;

/**
 * Created by VAIO on 6/1/2016.
 */
public class EditChallengeModel {
    private String nama_challenge;
    private String deskripsi;

    public String getNama_challenge() {
        return nama_challenge;
    }

    public void setNama_challenge(String nama_challenge) {
        this.nama_challenge = nama_challenge;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

}
