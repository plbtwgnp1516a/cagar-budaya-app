package com.example.herlin.projectplbtw.editreward.userinterface;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.herlin.projectplbtw.R;
import com.example.herlin.projectplbtw.ServiceGenerator;
import com.example.herlin.projectplbtw.editreward.api.ApiEditReward;
import com.example.herlin.projectplbtw.editreward.model.EditRewardModel;
import com.example.herlin.projectplbtw.editreward.model.EditRewardResponse;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class EditRewardActivity extends AppCompatActivity {

    private String id_reward;
    private String nama_reward;
    private String deskripsi;
    private String minimal_poin;

    private ApiEditReward apiEditReward = null;
    public static final String API_KEY="asdfghjkl";
    @Bind(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    @Bind(R.id.textNamaReward)
    EditText textNamaReward;
    @Bind(R.id.textDeskripsi)
    EditText textDeskripsi;
    @Bind(R.id.textMinimalPoin)
    EditText textMinimalPoin;
    @Bind(R.id.btnCancel)
    Button btnCancel;
    @Bind(R.id.btnEditReward)
    Button btnAddReward;
    @Bind(R.id.btnSelectImage)
    Button btnSelectImage;

    @OnClick(R.id.btnCancel)
    void backToMenu(View view) {
        this.finish();
    }
    @OnClick(R.id.btnEditReward)
    void setOnClickBtnAddReward(View view)
    {
        if (textMinimalPoin.getText().toString().isEmpty() || textDeskripsi.getText().toString().isEmpty() || textNamaReward.getText().toString().isEmpty()) {
            Snackbar.make(view, "Please fill the blank field!", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();

        }
        else
        {
            final ProgressDialog dialog = new ProgressDialog(EditRewardActivity.this);
            dialog.setMessage("Please wait...");
            dialog.setCancelable(false);
            dialog.show();
            Call<EditRewardResponse> call = null;

            EditRewardModel editRewardModel = new EditRewardModel();
            editRewardModel.setDeskripsi(textDeskripsi.getText().toString());
            editRewardModel.setNama_reward(textNamaReward.getText().toString());
            editRewardModel.setMinimal_poin(textMinimalPoin.getText().toString());

            call = apiEditReward.editReward(id_reward,editRewardModel);

            call.enqueue(new Callback<EditRewardResponse>() {
                @Override
                public void onResponse(Response<EditRewardResponse> response, Retrofit retrofit) {
                    dialog.dismiss();
                    Toast.makeText(EditRewardActivity.this, response.code() + "", Toast.LENGTH_SHORT).show();
                    if (2 == response.code() / 100) {
                        // Toast.makeText(LoginActivity.this, response.code() + " ", Toast.LENGTH_SHORT).show();
                        if (response.body().getSuccess() == true)
                            moveToDetail();

                        else
                            Snackbar.make(coordinatorLayout, "Edit Reward Error", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                    } else {
                        Snackbar.make(coordinatorLayout, "Edit Reward Error", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    dialog.dismiss();
                    Snackbar.make(coordinatorLayout, t.getMessage()+"Edit Reward Failed", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    //     textNamaCagar.setText(t.getMessage()+"");

                }
            });
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_reward);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Edit Herritage");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        apiEditReward = ServiceGenerator.createService(ApiEditReward.class);

        Bundle b = getIntent().getExtras();
        nama_reward=b.getString("nama_reward");
        deskripsi=b.getString("deskripsi");
        minimal_poin=b.getString("minimal_poin");
        id_reward=b.getString("id_reward");
        textNamaReward.setText(nama_reward);
        textDeskripsi.setText(deskripsi);
        textMinimalPoin.setText(minimal_poin);
    }
    private void moveToDetail()
    {
        finish();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
