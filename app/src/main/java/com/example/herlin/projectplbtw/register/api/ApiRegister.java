package com.example.herlin.projectplbtw.register.api;

import com.example.herlin.projectplbtw.register.model.RegisterModel;
import com.example.herlin.projectplbtw.register.model.RegisterResponse;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.Header;
import retrofit.http.Headers;
import retrofit.http.POST;

/**
 * Created by VAIO on 5/21/2016.
 */
public interface ApiRegister {
    @Headers({
            "Accept: application/json",
            "Content-type: application/json"
    })
    @POST("user/signup")
    Call<RegisterResponse> postRegister(@Body RegisterModel registerModel);

}
