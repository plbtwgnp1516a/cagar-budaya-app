package com.example.herlin.projectplbtw.addreward.model;

/**
 * Created by VAIO on 5/21/2016.
 */
public class AddRewardResponse {
    private Boolean Success;
    private String Info;

    public Boolean getSuccess() {
        return Success;
    }

    public void setSuccess(Boolean success) {
        Success = success;
    }

    public String getInfo() {
        return Info;
    }

    public void setInfo(String info) {
        Info = info;
    }

}
