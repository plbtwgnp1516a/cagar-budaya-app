package com.example.herlin.projectplbtw.addchallenge.model;

/**
 * Created by VAIO on 5/21/2016.
 */
public class AddChallengeModel {
    private String nama_challenge;
    private String deskripsi;


    public String getNama_challenge() {
        return nama_challenge;
    }

    public void setNama_challenge(String nama_challenge) {
        this.nama_challenge = nama_challenge;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }
}
