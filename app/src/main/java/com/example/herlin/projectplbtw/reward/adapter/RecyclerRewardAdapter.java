package com.example.herlin.projectplbtw.reward.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.herlin.projectplbtw.R;
import com.example.herlin.projectplbtw.detailreward.userinterface.DetailRewardActivity;
import com.example.herlin.projectplbtw.reward.api.ApiReward;
import com.example.herlin.projectplbtw.reward.model.RewardModel;
import com.example.herlin.projectplbtw.reward.userinterface.RewardActivity;
import com.example.herlin.projectplbtw.ServiceGenerator;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by VAIO on 5/16/2016.
 */
public class RecyclerRewardAdapter extends RecyclerView.Adapter {

    private static final String idUserMarker = "id_user_marker";
    private static final String loginMarker = "login_marker";
    private static final String namaMarker = "nama_marker";
    private static final String emailMarker = "email_marker";
    private static final String poinMarker = "poin_marker";
    private static final String teleponMarker = "telepon_marker";
    private static final String roleMarker = "role_marker";
    private static SharedPreferences sharedPreferences;
    private static final String sharedPreferenceName = "shared_preference";


    private RewardActivity rewardActivity;
    public ApiReward apiReward = null;
    private final int TYPE_REWARD = 1;
    private Context context;
    private LayoutInflater inflater;
    private List<RewardModel> rewardModelList;
    private CoordinatorLayout coordinatorLayout;
    private RewardViewHolder rewardViewHolder;
    public void setRewardActivity(RewardActivity rewardActivity) {
        this.rewardActivity = rewardActivity;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setInflater(LayoutInflater inflater) {
        this.inflater = inflater;
    }

    public RecyclerRewardAdapter
            (List<RewardModel> rewardModelList, RecyclerView recyclerView) {
        apiReward = ServiceGenerator.createService(ApiReward.class);
        this.rewardModelList = rewardModelList;

    }

    @Override
    public int getItemViewType(int position) {
        return TYPE_REWARD;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layoutId;
        View view;
        layoutId = R.layout.item_reward_list;
        view = LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);
        return new RewardViewHolder(view);

    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        setRewardViewHolder(holder, position);



    }

    public void setCoordinatorLayout(CoordinatorLayout coordinatorLayout) {
        this.coordinatorLayout = coordinatorLayout;
    }


    public void setRewardViewHolder(RecyclerView.ViewHolder holder,final int position) {
        rewardViewHolder = (RewardViewHolder) holder;


        rewardViewHolder.rewardModel = rewardModelList.get(position);

        sharedPreferences = holder.itemView.getContext().getSharedPreferences(sharedPreferenceName, 0);
        rewardViewHolder.textNamaReward.setText(rewardViewHolder
                .rewardModel.getNama_reward()+"");
        rewardViewHolder.textMinimalPoin.setText(rewardViewHolder
                .rewardModel.getMinimal_poin()+"");
        Picasso.with(holder.itemView.getContext()).load(rewardViewHolder.rewardModel.getId_reward())
                .placeholder(R.drawable.cabud4).into(rewardViewHolder.imageReward);

            rewardViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle b = new Bundle();
                    b.putString("nama_reward",rewardModelList.get(position).getNama_reward());
                    b.putString("deskripsi",rewardModelList.get(position).getDeskripsi());
                    b.putString("minimal_poin",rewardModelList.get(position).getMinimal_poin());
                    b.putString("id_reward",rewardModelList.get(position).getId_reward());


                    Intent i = new Intent(v.getContext(), DetailRewardActivity.class);
                    i.putExtras(b);
                    rewardActivity.startActivity(i);

                }
            });

    }

    @Override
    public int getItemCount() {
        return (rewardModelList.size());
    }


    public static class RewardViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.textNamaReward)
        TextView textNamaReward;
        @Bind(R.id.textMinimalPoin)
        TextView textMinimalPoin;
        @Bind(R.id.imageReward)
        ImageView imageReward;

        RewardModel rewardModel;

        public RewardViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }
}
