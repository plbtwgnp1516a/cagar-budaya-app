package com.example.herlin.projectplbtw.detailcagar.userinterface;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.herlin.projectplbtw.AdminActivity;
import com.example.herlin.projectplbtw.MainActivity;
import com.example.herlin.projectplbtw.R;
import com.example.herlin.projectplbtw.ServiceGenerator;
import com.example.herlin.projectplbtw.cagarbudaya.model.CagarModel;
import com.example.herlin.projectplbtw.cagarbudaya.userinterface.CagarListActivity;
import com.example.herlin.projectplbtw.detailcagar.api.ApiDetailCagar;
import com.example.herlin.projectplbtw.detailcagar.model.DetailCagarResponse;
import com.example.herlin.projectplbtw.editherritage.userinterface.EditHerritageActivity;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class DetailCagarActivity extends AppCompatActivity {
    private String nama_cagar;
    private String deskripsi;
    private String tahun;
    private String id_cagar;
    private String id_user;
    private String alamat;
    private String longitude;
    private String latitude;

    public static final String API_KEY="asdfghjkl";

    private static SharedPreferences sharedPreferences;
    private static final String sharedPreferenceName = "shared_preference";
    private static final String idUserMarker = "id_user_marker";
    private static final String loginMarker = "login_marker";
    private static final String namaMarker = "nama_marker";
    private static final String emailMarker = "email_marker";
    private static final String poinMarker = "poin_marker";
    private static final String teleponMarker = "telepon_marker";
    private static final String roleMarker = "role_marker";
    private ApiDetailCagar apiDetailCagar=null;

    @Bind(R.id.imageThumbnail)
    ImageView imageThumbnail;
    @Bind(R.id.textNamaCagar)
    TextView textNamaCagar;
    @Bind(R.id.textTahun)
    TextView textTahun;
    @Bind(R.id.textJenis)
    TextView textJenis;
    @Bind(R.id.textAlamat)
    TextView textAlamat;
    @Bind(R.id.textDeskripsi)
    TextView textDeskripsi;
    @Bind(R.id.btnDelete)
    Button btnDelete;
    @Bind(R.id.btnEdit)
    Button btnEdit;

    @OnClick(R.id.btnEdit)
    void setOnClickBtnEdit(View view)
    {
        Bundle b = new Bundle();
        b.putString("nama_cagar",nama_cagar);
        b.putString("deskripsi",deskripsi);
        b.putString("tahun",tahun);
        b.putString("id_cagar",id_cagar);
        b.putString("id_user",id_user);
        b.putString("alamat",alamat);
        b.putString("longitude",longitude);
        b.putString("latitude",latitude);

        Intent i = new Intent(getApplicationContext(), EditHerritageActivity.class);
        i.putExtras(b);
        startActivity(i);
    }

    @OnClick(R.id.btnDelete)
    void setOnClickButtonDelete(final View view)
    {
        final android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(this);
        alertDialog.setMessage(Html.fromHtml("<strong>Apakah anda yakin ingin menghapus cagar budaya ini?</strong>"));
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface d, int which) {
                final ProgressDialog dialog = new ProgressDialog(view.getContext());
                dialog.setMessage("Please wait...");
                dialog.show();

                Call<DetailCagarResponse> call = null;
                call = apiDetailCagar.deleteCagar(id_cagar,API_KEY);
                call.enqueue(new Callback<DetailCagarResponse>() {
                    @Override
                    public void onResponse(Response<DetailCagarResponse> response, Retrofit retrofit) {
                        Toast.makeText(DetailCagarActivity.this, response.code()+"", Toast.LENGTH_SHORT).show();

                        if (2 == response.code() / 100) {
                            Toast.makeText(DetailCagarActivity.this, "Cagar Budaya Terhapus", Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            dialog.dismiss();
                            Toast.makeText(DetailCagarActivity.this, "Error dalam menghapus Cagar Budaya ", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        dialog.dismiss();
                        Toast.makeText(DetailCagarActivity.this, "Gagal menghapus cagar budaya", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });
            }
        });
        alertDialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface d, int which) {
            }
        });
        alertDialog.show();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_cagar);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        apiDetailCagar = ServiceGenerator.createService(ApiDetailCagar.class);
        sharedPreferences = getSharedPreferences(sharedPreferenceName, 0);
        Bundle b = getIntent().getExtras();
        nama_cagar=b.getString("nama_cagar");
        deskripsi=b.getString("deskripsi");
        tahun=b.getString("tahun");
        id_cagar=b.getString("id_cagar");
        id_user=b.getString("id_user");
        alamat=b.getString("alamat");
        longitude=b.getString("longitude");
        latitude=b.getString("latitude");
        textNamaCagar.setText(nama_cagar);
        textDeskripsi.setText(deskripsi);
        textTahun.setText(tahun);
        textAlamat.setText(alamat);


        if(sharedPreferences.getString(roleMarker,"").equalsIgnoreCase("1")) {
            btnDelete.setVisibility(View.INVISIBLE);
            btnEdit.setVisibility(View.INVISIBLE);
        }

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Intent intent = new Intent();
            setResult(1, intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
