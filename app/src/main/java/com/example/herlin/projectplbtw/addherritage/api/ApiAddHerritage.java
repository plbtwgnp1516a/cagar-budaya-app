package com.example.herlin.projectplbtw.addherritage.api;

import com.example.herlin.projectplbtw.addherritage.model.AddHerritageModel;
import com.example.herlin.projectplbtw.addherritage.model.AddHerritageResponse;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by VAIO on 5/21/2016.
 */
public interface ApiAddHerritage {
    @POST("cagarbudaya")
    Call<AddHerritageResponse> postHerritage(@Body AddHerritageModel addHerritageModel);
}
