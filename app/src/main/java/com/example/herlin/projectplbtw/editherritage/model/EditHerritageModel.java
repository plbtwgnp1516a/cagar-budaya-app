package com.example.herlin.projectplbtw.editherritage.model;

/**
 * Created by VAIO on 6/1/2016.
 */
public class EditHerritageModel {
    private String api_key;
    private String nama_cagar;
    private String deskripsi;
    private String tahun_peninggalan;
    private String nama_jenis;
    private String id_user;
    private String longitude;
    private String latitude;
    private String alamat;
    private String status;

    public String getNama_cagar() {
        return nama_cagar;
    }

    public void setNama_cagar(String nama_cagar) {
        this.nama_cagar = nama_cagar;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getTahun_peninggalan() {
        return tahun_peninggalan;
    }

    public void setTahun_peninggalan(String tahun_peninggalan) {
        this.tahun_peninggalan = tahun_peninggalan;
    }

    public String getNama_jenis() {
        return nama_jenis;
    }

    public void setNama_jenis(String nama_jenis) {
        this.nama_jenis = nama_jenis;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getApi_key() {
        return api_key;
    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }
}
