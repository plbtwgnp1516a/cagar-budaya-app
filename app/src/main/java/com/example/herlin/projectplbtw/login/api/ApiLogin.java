package com.example.herlin.projectplbtw.login.api;

import com.example.herlin.projectplbtw.login.model.LoginModel;
import com.example.herlin.projectplbtw.login.model.LoginResponse;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by VAIO on 5/21/2016.
 */
public interface ApiLogin {
    @POST("loginuser")
    Call<LoginResponse> postLogin(@Body LoginModel userLogin);
}
