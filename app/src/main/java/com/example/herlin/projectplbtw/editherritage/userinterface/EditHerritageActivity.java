package com.example.herlin.projectplbtw.editherritage.userinterface;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.herlin.projectplbtw.R;
import com.example.herlin.projectplbtw.ServiceGenerator;
import com.example.herlin.projectplbtw.editherritage.api.ApiEditHerritage;
import com.example.herlin.projectplbtw.editherritage.model.EditHerritageModel;
import com.example.herlin.projectplbtw.editherritage.model.EditHerritageResponse;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class EditHerritageActivity extends AppCompatActivity {

    private String nama_cagar;
    private String deskripsi;
    private String tahun;
    private String id_cagar;
    private String id_user;
    private String alamat;
    private String longitude;
    private String latitude;


    private ApiEditHerritage apiEditHerritage = null;
    public static final String API_KEY="asdfghjkl";
    @Bind(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    @Bind(R.id.textNamaCagar)
    EditText textNamaCagar;
    @Bind(R.id.textDeskripsi)
    EditText textDeskripsi;
    @Bind(R.id.textTahun)
    EditText textTahun;
    @Bind(R.id.spinJenis)
    Spinner spinJenis;
    @Bind(R.id.textAlamat)
    EditText textAlamat;
    @Bind(R.id.imageThumbnail)
    ImageView imageThumbnail;
    @Bind(R.id.btnCancel)
    Button btnCancel;
    @Bind(R.id.btnEditCagar)
    Button btnEditCagar;
    @Bind(R.id.btnSelectImage)
    Button btnSelectImage;

    @OnClick(R.id.btnCancel)
    void backToMenu(View view) {
        this.finish();
    }

    @OnClick(R.id.btnEditCagar)
    void setOnClickBtnEditCagar(View view) {
        if (textAlamat.getText().toString().isEmpty() || textDeskripsi.getText().toString().isEmpty() || textNamaCagar.getText().toString().isEmpty() || textTahun.getText().toString().isEmpty()) {
            Snackbar.make(view, "Please fill the blank field!", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();

        }
        else
        {
            final ProgressDialog dialog = new ProgressDialog(EditHerritageActivity.this);
            dialog.setMessage("Please wait...");
            dialog.setCancelable(false);
            dialog.show();
            Call<EditHerritageResponse> call = null;
            EditHerritageModel editHerritageModel = new EditHerritageModel();
            editHerritageModel.setApi_key(API_KEY);
            editHerritageModel.setAlamat(textAlamat.getText().toString());
            editHerritageModel.setDeskripsi(textDeskripsi.getText().toString());
            editHerritageModel.setId_user("1"); //harus dr shared preference
            editHerritageModel.setLatitude("123"); //harus dr maps
            editHerritageModel.setLongitude("456");  //harus dr maps
            editHerritageModel.setNama_cagar(textNamaCagar.getText().toString());
            editHerritageModel.setStatus("waiting for confirmation");
            editHerritageModel.setTahun_peninggalan(textTahun.getText().toString());
            editHerritageModel.setNama_jenis(spinJenis.getSelectedItem().toString());

            call = apiEditHerritage.editHerritage(id_cagar,editHerritageModel);

            call.enqueue(new Callback<EditHerritageResponse>() {
                @Override
                public void onResponse(Response<EditHerritageResponse> response, Retrofit retrofit) {
                    dialog.dismiss();
                    Toast.makeText(EditHerritageActivity.this, response.code() + "", Toast.LENGTH_SHORT).show();
                    if (2 == response.code() / 100) {
                        // Toast.makeText(LoginActivity.this, response.code() + " ", Toast.LENGTH_SHORT).show();
                        if (response.body().getSuccess() == true)
                            moveToMenu();

                        else
                            Snackbar.make(coordinatorLayout, "Edit Herritage Error", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                    } else {
                        Snackbar.make(coordinatorLayout, "Edit Herritage Error", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    dialog.dismiss();
                    Snackbar.make(coordinatorLayout, t.getMessage()+"Edit Herritage Failed", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    //     textNamaCagar.setText(t.getMessage()+"");

                }
            });
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_herritage);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Edit Herritage");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        apiEditHerritage = ServiceGenerator.createService(ApiEditHerritage.class);

        Bundle b = getIntent().getExtras();
        nama_cagar=b.getString("nama_cagar");
        deskripsi=b.getString("deskripsi");
        tahun=b.getString("tahun");
        id_cagar=b.getString("id_cagar");
        id_user=b.getString("id_user");
        alamat=b.getString("alamat");
        longitude=b.getString("longitude");
        latitude=b.getString("latitude");
        textNamaCagar.setText(nama_cagar);
        textDeskripsi.setText(deskripsi);
        textTahun.setText(tahun);
        textAlamat.setText(alamat);

    }

    private void moveToMenu()
    {
        Toast.makeText(EditHerritageActivity.this, "Successfully edit herritage!", Toast.LENGTH_SHORT).show();
        finish();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
