package com.example.herlin.projectplbtw.detailchallenge.api;

import com.example.herlin.projectplbtw.detailchallenge.model.DetailChallengeResponse;

import retrofit.Call;
import retrofit.http.DELETE;
import retrofit.http.Path;

/**
 * Created by VAIO on 5/30/2016.
 */
public interface ApiDetailChallenge {
    @DELETE("challenge/{id}")
    Call<DetailChallengeResponse> deleteChallenge(@Path("id") String id);
}
