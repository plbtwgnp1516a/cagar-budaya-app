package com.example.herlin.projectplbtw.detailcagar.api;

import com.example.herlin.projectplbtw.cagarbudaya.model.CagarModel;
import com.example.herlin.projectplbtw.detailcagar.model.DetailCagarResponse;

import retrofit.Call;
import retrofit.http.DELETE;
import retrofit.http.Header;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by VAIO on 5/28/2016.
 */
public interface ApiDetailCagar {
    @DELETE("cagarbudaya/{id}")
    Call<DetailCagarResponse> deleteCagar(@Path("id") String id,@Query("api_key") String apiKey);
}
