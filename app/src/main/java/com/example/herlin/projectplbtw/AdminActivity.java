package com.example.herlin.projectplbtw;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.herlin.projectplbtw.aboutus.AboutUsActivity;
import com.example.herlin.projectplbtw.cagarbudaya.userinterface.CagarListActivity;
import com.example.herlin.projectplbtw.challenge.userinterface.ChallengeActivity;
import com.example.herlin.projectplbtw.login.userinterface.LoginActivity;
import com.example.herlin.projectplbtw.reward.userinterface.RewardActivity;

/**
 * Created by VAIO on 5/23/2016.
 */
public class AdminActivity  extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String idUserMarker = "id_user_marker";
    private static final String loginMarker = "login_marker";
    private static final String namaMarker = "nama_marker";
    private static final String emailMarker = "email_marker";
    private static final String poinMarker = "poin_marker";
    private static final String teleponMarker = "telepon_marker";

    private static final String roleMarker = "role_marker";
    private static SharedPreferences sharedPreferences;
    private static final String sharedPreferenceName = "shared_preference";


    Fragment about= new AboutUsActivity();
    Fragment cagar = new CagarListActivity();
    Fragment challenge = new ChallengeActivity();
    Fragment reward = new RewardActivity();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        sharedPreferences = getSharedPreferences(sharedPreferenceName, 0);
        Toast.makeText(AdminActivity.this, sharedPreferences.getString(roleMarker,"nol")+"", Toast.LENGTH_SHORT).show();


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        Toast.makeText(AdminActivity.this, "masuk admin", Toast.LENGTH_SHORT).show();
        Bundle b = getIntent().getExtras();
        if(b!=null)
        {
            Toast.makeText(AdminActivity.this, "masuk bundle", Toast.LENGTH_SHORT).show();
            String cek = b.getString("cagar","");
            if(cek.equalsIgnoreCase("true"))
            {
                Toast.makeText(AdminActivity.this, "masuk cagar true", Toast.LENGTH_SHORT).show();

                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, cagar)
                        .commit();
            }
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, cagar)
                .commit();

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
      //  Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.herritage);
      //  fragment.onActivityResult(requestCode, resultCode, data);
        Toast.makeText(AdminActivity.this, "masuk dr admin", Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.about_us) {
            // Handle the camera action
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, about)
                    .commit();
        } else if (id == R.id.herritage) {

            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, cagar)
                    .commit();

        } else if (id == R.id.reward) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, reward)
                    .commit();

        }else if (id == R.id.challenge) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, challenge)
                    .commit();

        }else if (id == R.id.logout) {
            clearSharedPreference();
            Intent i = new Intent(getApplicationContext(),LoginActivity.class);
            startActivity(i);
            this.finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void clearSharedPreference() {
        SharedPreferences.Editor e = sharedPreferences.edit();
        e.putBoolean(this.loginMarker, false);
        e.putString(namaMarker,"");
        e.putString(emailMarker,"");
        e.putString(poinMarker,"");
        e.putString(teleponMarker,"");
        e.putString(roleMarker,"");
        e.putString(idUserMarker,"");
        e.commit();
    }
}
