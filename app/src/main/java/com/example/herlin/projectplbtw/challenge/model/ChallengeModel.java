package com.example.herlin.projectplbtw.challenge.model;

/**
 * Created by VAIO on 5/17/2016.
 */
public class ChallengeModel {
    private String id_challenge;
    private String nama_challenge;
    private String deskripsi;
 //   private String image_challenge;

    public String getId_challenge() {
        return id_challenge;
    }

    public void setId_challenge(String id_challenge) {
        this.id_challenge = id_challenge;
    }

    public String getNama_challenge() {
        return nama_challenge;
    }

    public void setNama_challenge(String nama_challenge) {
        this.nama_challenge = nama_challenge;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }
}
