package com.example.herlin.projectplbtw.cagarbudaya.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.herlin.projectplbtw.OnLoadMoreListener;
import com.example.herlin.projectplbtw.R;
import com.example.herlin.projectplbtw.ServiceGenerator;
import com.example.herlin.projectplbtw.cagarbudaya.api.ApiCagarList;
import com.example.herlin.projectplbtw.cagarbudaya.model.CagarModel;
import com.example.herlin.projectplbtw.cagarbudaya.userinterface.CagarListActivity;
import com.example.herlin.projectplbtw.detailcagar.userinterface.DetailCagarActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by VAIO on 5/14/2016.
 */
public class RecyclerAdapterCagar extends RecyclerView.Adapter {
    private static final String idUserMarker = "id_user_marker";
    private static final String loginMarker = "login_marker";
    private static final String namaMarker = "nama_marker";
    private static final String emailMarker = "email_marker";
    private static final String poinMarker = "poin_marker";
    private static final String teleponMarker = "telepon_marker";
    private static final String roleMarker = "role_marker";
    private static SharedPreferences sharedPreferences;
    private static final String sharedPreferenceName = "shared_preference";


    private CagarListActivity cagarListActivity;
    public ApiCagarList apiCagarList = null;
    private final int TYPE_CAGAR = 1;
    private final int TYPE_LOAD_MORE = 2;


    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;

    private int visibleThreshold = 1;   //determine when load more will run. "1" indicates that
    //load more will run when user scroll on the last article

    private Context context;
    private LayoutInflater inflater;
    private List<CagarModel> cagarModelList;
    private CoordinatorLayout coordinatorLayout;
    private CagarViewHolder cagarViewHolder;
    public void setCagarListActivity(CagarListActivity cagarListActivity) {
        this.cagarListActivity = cagarListActivity;
    }

    public void setContext(Context context) {
        this.context = context;
    }
    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }
    public void setInflater(LayoutInflater inflater) {
        this.inflater = inflater;
    }

    public void setLoaded() {
        loading = false;
    }
    public RecyclerAdapterCagar
            (List<CagarModel> cagarModelList, RecyclerView recyclerView) {
        apiCagarList = ServiceGenerator.createService(ApiCagarList.class);
        this.cagarModelList = cagarModelList;

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();

            recyclerView
                    .addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView,
                                               int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);

                            totalItemCount = linearLayoutManager.getItemCount();
                            lastVisibleItem = linearLayoutManager
                                    .findLastVisibleItemPosition();
                            if (!loading
                                    && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                                // End has been reached
                                // Do something
                                if (onLoadMoreListener != null) {
                                    onLoadMoreListener.onLoadMore();
                                }
                                loading = true;
                            }
                        }
                    });
        }

    }

    @Override
    public int getItemViewType(int position) {
        if(cagarModelList.get(position) != null)
           return TYPE_CAGAR;
        else
            return TYPE_LOAD_MORE;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layoutId;
        View view;
        switch (viewType) {
            case TYPE_CAGAR:
            layoutId = R.layout.item_cagar_list;
            view = LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);
            return new CagarViewHolder(view);
            case TYPE_LOAD_MORE:
                layoutId = R.layout.progress_item;
                view = LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);
                return new ProgressViewHolder(view);
        }
        return null;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if(holder instanceof CagarViewHolder)
            setCagarViewHolder(holder, position);
        else
            setProgressBarViewHolder(holder);


    }
    public void setProgressBarViewHolder(RecyclerView.ViewHolder holder) {
        ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
    }

    public void setCoordinatorLayout(CoordinatorLayout coordinatorLayout) {
        this.coordinatorLayout = coordinatorLayout;
    }


    public void setCagarViewHolder(RecyclerView.ViewHolder holder, final int position) {

        cagarViewHolder = (CagarViewHolder) holder;
        cagarViewHolder.cagarModel = cagarModelList.get(position);
        sharedPreferences = holder.itemView.getContext().getSharedPreferences(sharedPreferenceName, 0);
        cagarViewHolder.textNamaCagar.setText(cagarViewHolder
                .cagarModel.getNama_cagar()+"");
        cagarViewHolder.textTahun.setText(cagarViewHolder
                .cagarModel.getTahun_peninggalan()+"");
        Picasso.with(holder.itemView.getContext()).load(cagarViewHolder.cagarModel.getId_multimedia())
                .placeholder(R.drawable.cabud4).into(cagarViewHolder.imageCagar);

            cagarViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle b = new Bundle();
                    b.putString("nama_cagar",cagarModelList.get(position).getNama_cagar());
                    b.putString("deskripsi",cagarModelList.get(position).getDeskripsi());
                    b.putString("tahun",cagarModelList.get(position).getTahun_peninggalan());
                    b.putString("id_cagar",cagarModelList.get(position).getId_cagar());
                    b.putString("id_user",cagarModelList.get(position).getId_user());
                    b.putString("alamat",cagarModelList.get(position).getAlamat());
                    b.putString("longitude",cagarModelList.get(position).getLongitude());
                    b.putString("latitude",cagarModelList.get(position).getLatitude());

                    Intent i = new Intent(v.getContext(), DetailCagarActivity.class);
                    i.putExtras(b);
                    cagarListActivity.startActivity(i);

//                    cagarListActivity.startActivityForResult(i,1);
              //      ((Activity) context).finish();
                }
            });
    }

    @Override
    public int getItemCount() {
        return (cagarModelList.size());
    }


    public static class CagarViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.textNamaCagar)
        TextView textNamaCagar;
        @Bind(R.id.textTahun)
        TextView textTahun;
        @Bind(R.id.imageCagar)
        ImageView imageCagar;

        CagarModel cagarModel;

        public CagarViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }
    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.progressBar1)
        ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, itemView);
        }
    }
}
