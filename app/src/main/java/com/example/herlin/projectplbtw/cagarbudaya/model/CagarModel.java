package com.example.herlin.projectplbtw.cagarbudaya.model;

/**
 * Created by VAIO on 5/14/2016.
 */
public class CagarModel {
    private String id_cagar;
    private String nama_cagar;
    private String deskripsi;
    private String tahun_peninggalan;
    private String nama_jenis;
    private String id_multimedia;
    private String id_user;
    private String alamat;
    private String longitude;
    private String latitude;

    public String getId_cagar() {
        return id_cagar;
    }

    public void setId_cagar(String id_cagar) {
        this.id_cagar = id_cagar;
    }

    public String getNama_cagar() {
        return nama_cagar;
    }

    public void setNama_cagar(String nama_cagar) {
        this.nama_cagar = nama_cagar;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getTahun_peninggalan() {
        return tahun_peninggalan;
    }

    public void setTahun_peninggalan(String tahun_peninggalan) {
        this.tahun_peninggalan = tahun_peninggalan;
    }


    public String getId_multimedia() {
        return id_multimedia;
    }

    public void setId_multimedia(String id_multimedia) {
        this.id_multimedia = id_multimedia;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getNama_jenis() {
        return nama_jenis;
    }

    public void setNama_jenis(String nama_jenis) {
        this.nama_jenis = nama_jenis;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }
}
