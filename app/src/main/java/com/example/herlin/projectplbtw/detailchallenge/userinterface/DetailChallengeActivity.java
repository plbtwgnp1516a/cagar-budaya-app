package com.example.herlin.projectplbtw.detailchallenge.userinterface;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.herlin.projectplbtw.R;
import com.example.herlin.projectplbtw.ServiceGenerator;
import com.example.herlin.projectplbtw.detailchallenge.api.ApiDetailChallenge;
import com.example.herlin.projectplbtw.detailchallenge.model.DetailChallengeResponse;
import com.example.herlin.projectplbtw.editchallenge.userinterface.EditChallengeActivity;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class DetailChallengeActivity extends AppCompatActivity {
    private String nama_challenge;
    private String deskripsi;
    private String id_challenge;


    private static SharedPreferences sharedPreferences;
    private static final String sharedPreferenceName = "shared_preference";
    private static final String idUserMarker = "id_user_marker";
    private static final String loginMarker = "login_marker";
    private static final String namaMarker = "nama_marker";
    private static final String emailMarker = "email_marker";
    private static final String poinMarker = "poin_marker";
    private static final String teleponMarker = "telepon_marker";
    private static final String roleMarker = "role_marker";
    private ApiDetailChallenge apiDetailChallenge =null;



    @Bind(R.id.imageThumbnail)
    ImageView imageThumbnail;
    @Bind(R.id.textNamaChallenge)
    TextView textNamaChallenge;
    @Bind(R.id.textDeskripsi)
    TextView textDeskripsi;
    @Bind(R.id.btnDelete)
    Button btnDelete;
    @Bind(R.id.btnEdit)
    Button btnEdit;
    @OnClick(R.id.btnEdit)
    void setOnClickBtnEdit(View view)
    {
        Bundle b = new Bundle();
        b.putString("nama_challenge",nama_challenge);
        b.putString("deskripsi_challenge",deskripsi);
        b.putString("id_challenge",id_challenge);

        Intent i = new Intent(getApplicationContext(), EditChallengeActivity.class);
        i.putExtras(b);
        startActivity(i);
    }

    @OnClick(R.id.btnDelete)
    void setOnClickButtonDelete(final View view)
    {
        final android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(this);
        alertDialog.setMessage(Html.fromHtml("<strong>Apakah anda yakin ingin menghapus challenge ini?</strong>"));
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface d, int which) {
                final ProgressDialog dialog = new ProgressDialog(view.getContext());
                dialog.setMessage("Please wait...");
                dialog.show();

                Call<DetailChallengeResponse> call = null;
                call = apiDetailChallenge.deleteChallenge(id_challenge);
                call.enqueue(new Callback<DetailChallengeResponse>() {
                    @Override
                    public void onResponse(Response<DetailChallengeResponse> response, Retrofit retrofit) {
                        Toast.makeText(DetailChallengeActivity.this, response.code()+"", Toast.LENGTH_SHORT).show();

                        if (2 == response.code() / 100) {

                            Toast.makeText(DetailChallengeActivity.this, "Cagar Budaya Terhapus", Toast.LENGTH_SHORT).show();

                            finish();
                        } else {
                            dialog.dismiss();
                            Toast.makeText(DetailChallengeActivity.this, "Error dalam menghapus Cagar Budaya ", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        dialog.dismiss();
                        Toast.makeText(DetailChallengeActivity.this, "Gagal menghapus cagar budaya", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });
            }
        });
        alertDialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface d, int which) {
            }
        });
        alertDialog.show();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_challenge);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        apiDetailChallenge = ServiceGenerator.createService(ApiDetailChallenge.class);
        sharedPreferences = getSharedPreferences(sharedPreferenceName, 0);
        Bundle b = getIntent().getExtras();
        nama_challenge=b.getString("nama_challenge");
        id_challenge=b.getString("id_challenge");
        deskripsi=b.getString("deskripsi");
        if(sharedPreferences.getString(roleMarker,"").equalsIgnoreCase("1")) {
            btnDelete.setVisibility(View.INVISIBLE);
            btnEdit.setVisibility(View.INVISIBLE);
        }

        textNamaChallenge.setText(nama_challenge);
        textDeskripsi.setText(deskripsi);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
