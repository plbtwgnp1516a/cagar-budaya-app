package com.example.herlin.projectplbtw.login.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by VAIO on 5/22/2016.
 */
public class LoginResponse {
  @SerializedName("content")
    private ArrayList<LoginObjectResponse> content=new ArrayList<LoginObjectResponse>();
    private String Info;

    public String getInfo() {
        return Info;
    }

    public void setInfo(String info) {
        this.Info = info;
    }
    public ArrayList<LoginObjectResponse> getContent() {
        return content;
    }

    public void setContent(ArrayList<LoginObjectResponse> content) {
        this.content = content;
    }
}
