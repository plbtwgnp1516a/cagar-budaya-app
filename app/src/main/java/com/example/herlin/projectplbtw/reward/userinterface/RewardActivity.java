package com.example.herlin.projectplbtw.reward.userinterface;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.herlin.projectplbtw.R;
import com.example.herlin.projectplbtw.addreward.userinterface.AddRewardActivity;
import com.example.herlin.projectplbtw.reward.adapter.RecyclerRewardAdapter;
import com.example.herlin.projectplbtw.reward.api.ApiReward;
import com.example.herlin.projectplbtw.reward.model.RewardModel;
import com.example.herlin.projectplbtw.reward.model.RewardResponse;
import com.example.herlin.projectplbtw.ServiceGenerator;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class RewardActivity extends Fragment {
    private static final String idUserMarker = "id_user_marker";
    private static final String loginMarker = "login_marker";
    private static final String namaMarker = "nama_marker";
    private static final String emailMarker = "email_marker";
    private static final String poinMarker = "poin_marker";
    private static final String teleponMarker = "telepon_marker";

    private static final String roleMarker = "role_marker";
    private static SharedPreferences sharedPreferences;
    private static final String sharedPreferenceName = "shared_preference";



    View rootview;
    public static final String API_KEY="asdfghjkl";
    @Bind(R.id.progressBar)
    ProgressBar progressBar;
    @Bind(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
  //  @Bind(R.id.contentRewardList)
  //  LinearLayout contentRewardList;

    @Bind(R.id.listReward)
    RecyclerView recyclerView;
    @Bind(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @Bind(R.id.fab)
    FloatingActionButton fab;
    @OnClick(R.id.fab)
    void setOnClickfab(View view)
    {
        Intent i=new Intent(getContext().getApplicationContext(), AddRewardActivity.class);
        startActivity(i);
    }

    private List<RewardModel> rewardModelList;
    private ApiReward apiReward = null;
    private RecyclerRewardAdapter adapter;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreate(savedInstanceState);
        rootview= inflater.inflate(R.layout.activity_reward, container, false);
        getActivity().setTitle("Reward");
        ButterKnife.bind(this, rootview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(
                new LinearLayoutManager(getActivity().getApplicationContext()));


        sharedPreferences = getActivity().getSharedPreferences(sharedPreferenceName, 0);
        String role = sharedPreferences.getString(roleMarker,"0");
        if(role.equalsIgnoreCase("2"))
        {
            fab.setVisibility(View.VISIBLE);
        }
        else
        {
            fab.setVisibility(View.INVISIBLE);
        }
     //   progressBar.setVisibility(View.VISIBLE);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (adapter == null) {
                    progressBar.setVisibility(View.VISIBLE);
                }
                getRewardList();
            }
        });
        apiReward = ServiceGenerator
                .createService(ApiReward.class);
        getRewardList();

        return rootview;
    }

    @Override
    public void onStart() {
        super.onStart();
        getRewardList();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void refreshPushNotification() {
        progressBar.setVisibility(View.VISIBLE);
   //     contentRewardList.setVisibility(View.INVISIBLE);
        getRewardList();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (9 == resultCode && 9 == requestCode) {
            refreshPushNotification();
            Snackbar.make(coordinatorLayout, "Push Notification Edited", Snackbar.LENGTH_LONG).show();
        }
    }

    private void getRewardList() {
        progressBar.setVisibility(View.VISIBLE);
        Call<RewardResponse> call = null;
        call = apiReward.getRewardList("1","1000",API_KEY);

//        call = apiReward.getHerritageList();
        call.enqueue(new Callback<RewardResponse>() {
            @Override
            public void onResponse(Response<RewardResponse> response,
                                   Retrofit retrofit) {

                if (2 == response.code() / 100) {

                    showRewardList(response);
                } else {
                    showErrorMessage();
                }
                progressBar.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
    //            contentRewardList.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Throwable t) {
                Snackbar.make(coordinatorLayout, "Failed to Load Content",
                        Snackbar.LENGTH_LONG).show();
                progressBar.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
      //          contentRewardList.setVisibility(View.VISIBLE);
            }
        });
    }

    private void showErrorMessage() {
        Snackbar.make(coordinatorLayout, "Error to Load Content", Snackbar.LENGTH_LONG).show();
    }

    private void showRewardList
            (Response<RewardResponse> response) {

        final RewardResponse rewardResponse = response.body();
        rewardModelList = rewardResponse.getListReward();
     //   if(rewardModelList!=null) {
            adapter = new RecyclerRewardAdapter(rewardModelList, recyclerView);
            adapter.setCoordinatorLayout(coordinatorLayout);
            adapter.setContext(getContext().getApplicationContext());
            adapter.setInflater(getActivity().getLayoutInflater());
            adapter.setRewardActivity(this);
            recyclerView.setAdapter(adapter);
       // }
    }

}
