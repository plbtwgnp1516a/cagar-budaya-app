package com.example.herlin.projectplbtw.challenge.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by VAIO on 5/17/2016.
 */
public class ChallengeResponse {
    @SerializedName("content")
    private ArrayList<ChallengeModel> listChallenge = new ArrayList<>();

    public ArrayList<ChallengeModel> getListChallenge() {
        return listChallenge;
    }

    public void setListChallenge(ArrayList<ChallengeModel> listChallenge) {
        this.listChallenge = listChallenge;
    }
}
