package com.example.herlin.projectplbtw.reward.api;

import com.example.herlin.projectplbtw.reward.model.RewardResponse;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by VAIO on 5/16/2016.
 */
public interface ApiReward {
    @GET("rewards/{page}/{size}")
    Call<RewardResponse>
    getRewardList(@Path("page") String page, @Path("size") String size, @Query("api_key") String apiKey);
}
