package com.example.herlin.projectplbtw.cagarbudaya.api;

import com.example.herlin.projectplbtw.cagarbudaya.model.CagarListResponse;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by VAIO on 5/14/2016.
 */
public interface ApiCagarList {

    @GET("cagarbudaya/{page}/{size}")
    Call<CagarListResponse>
    getHerritageList(@Path("page") String page,@Path("size") String size,@Query("api_key") String apiKey);

/*
    @GET("cagarbudaya/1/5")
    Call<CagarListResponse>
    getHerritageList();
*/
}
